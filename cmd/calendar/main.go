package main

import (
	"net/http"
	"time"

	c "gitlab.com/otus-go-hw/homework-15/internal/calendar"
	"gitlab.com/otus-go-hw/homework-15/pkg/configer"
	"gitlab.com/otus-go-hw/homework-15/pkg/logger"
)

func main() {
	cfg := configer.ReadConfig()
	log := logger.NewLogger(cfg.LogFile, cfg.LogLevel)
	calendar := c.NewCalendar()

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		log.Println("hello")
	})

	http.HandleFunc("/add", func(w http.ResponseWriter, r *http.Request) {
		event := &c.Event{
			Id:          1,
			Title:       "Morning coffee",
			Description: "The most important event of the day",
			Date:        time.Now(),
		}
		_ = calendar.Add(event)
		log.Println("added new event")
	})
	err := http.ListenAndServe(cfg.HttpListen, nil)
	if err != nil {
		log.Fatalf("http server error: %v", err)
	}
}
